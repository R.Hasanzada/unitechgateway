package az.ruslan.unitech.unitechgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniTechGateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(UniTechGateWayApplication.class, args);
    }

}
