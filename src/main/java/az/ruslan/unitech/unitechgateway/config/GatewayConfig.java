package az.ruslan.unitech.unitechgateway.config;

import az.ruslan.unitech.unitechgateway.jwt.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class GatewayConfig {

    @Autowired
    private JwtUtil jwtUtil;

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/auth/**","/register/**" )
                        .uri("http://localhost:8081"))  // UniTechLoginRegistration microservice
                .route(r -> r.path("/transfer/**", "/accounts/**")
                        .filters(f -> f.filter(new JwtAuthenticationFilter(jwtUtil)))
                        .uri("http://localhost:8082"))  // UniTechAccounts microservice
                .route(r -> r.path("/exchange/**")
                        .uri("http://localhost:8083"))  // UniTechCurrency microservice
                .build();
    }
}
